# Distributed Mathseer Extraction Pipeline

The Mathseer extraction pipeline is intended to be able to extract information from large collections of PDF documents. At some point a collection can become so large that the time needed to index it becomes prohibitive when limited by the computational power of a single machine. 

The solution to this problem is to use multiple machines at once (a computational cluster) to increase the volume in which PDFs can be processed. This README details how to do that with the Mathseer pipeline, using Docker and Kubernetes (k8s).

This is a more advanced guide and is intended to be a "production" tool. To use this version of the pipeline, you must have multiple computers at your disposal, preferably Linux based, at least two of which must have GPUs. It is also helpful if you have some experience with Docker, although it is not 100% necessary. 

If you are interested in debugging or running smaller tests against the mathseer pipeline it is recommended that you see the main README, and stick with the standard pipeline.

## Getting Started: Setting Up Kubernetes
The following steps show how to set up the Kubernetes cluster. We use the configuration of our own cluster so that a concrete example can be provided, some variables will have to be changed to make this work with your setup.
Our cluster consists of 7 machines: bob, doug, northbay, niagara, kitimat, halifax, and saskatoon. Saskatoon will be the leader node. All of our machines run Ubuntu 20.04.
These commands are a culmination of commands taken from https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/, https://github.com/flannel-io/flannel/blob/master/Documentation/kubernetes.md, https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html, and https://github.com/NVIDIA/k8s-device-plugin. All of these documents should be read in full before starting the installation process. Installing Kubernetes can be a fairly involved process. There may be additional or alternative steps that you need to take depending on the OS you are running. If any steps result in error messages it is best to consult the official k8s, docker, nvidia, or flannel documentation or forums.

### On Every Node
The following commands where run on all nodes prior to any of the master specific commands being run on saskatoon. 
- Install Docker: https://docs.docker.com/get-docker/

- add user to docker group `sudo usermod -aG docker devops`

- Install docker nvidia
  -  ```
    distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
    && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add 
    && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list```
  
  -  ```
       curl -s -L https://nvidia.github.io/nvidia-container-runtime/experimental/$distribution/nvidia-container-runtime.list | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
       ```
  
  -  `sudo apt-get update`
  
  -  `sudo apt-get install -y nvidia-docker2`
  
  -  `sudo systemctl restart docker`
  
- Kubernetes requires swap to be turned off. Run the following commands

  - `sudo swapoff -a`
  - `sed -i '/swap/d' /etc/fstab`

- Allow the nodes to see bridged networking

  - ```
    cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
    br_netfilter
    EOF
    ```

  - ```
    cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
    net.bridge.bridge-nf-call-ip6tables = 1
    net.bridge.bridge-nf-call-iptables = 1
    EOF
    ```

  - `sudo sysctl --system`

- Now that docker and docker nvidia are installed set the `/etc/docker/daemon.json` to 

  ```
  {
      "default-runtime": "nvidia",
      "runtimes": {
          "nvidia": {
              "path": "/usr/bin/nvidia-container-runtime",
              "runtimeArgs": []
          }
      },
      "exec-opts": ["native.cgroupdriver=systemd"]
  }
  ```

​		This sets the default docker runtime to nvidia which is necessary to run GPU utilizing containers. It also sets the cgroup to systemd which kubernetes 		wants
- Install Kubernetes dependencies

  - ```
    sudo apt-get update
    sudo apt-get install -y apt-transport-https ca-certificates curl
    ```

  - ```sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
    sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
    ```

  - ```
    echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
    ```

  - ```
    sudo apt-get install -y kubelet kubeadm kubectl
    sudo apt-mark hold kubelet kubeadm kubectl
    ```

- Now to make sure docker has all the daemon udpates:

  ```
  sudo systemctl daemon-reload
  sudo systemctl restart docker
  ```

### On the Master Node

- 

- Install Conda: https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html
## Configuring The Distributed Pipeline
The source of truth for the distributed 
## Running The Distributed Pipeline



