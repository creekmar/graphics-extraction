import os
import random
import os.path as osp

import cv2
import itertools
import networkx as nx
import pytest

from qdgga.src.utils.branchings_custom import maximum_spanning_arborescence_custom
from qdgga.src.datasets.math_symbol import INFRELATIONS

random.seed(16)

def construct_graph(G, sym_ids):
    G.add_nodes_from(sym_ids)
    G.add_edges_from(itertools.permutations(sym_ids, 2))
    weights = (i for i in range(6))
    for i, (p_id, c_id) in enumerate(G.edges()):
        #avoid self loops
        if p_id == c_id:
            continue
        G[p_id][c_id]['WEIGHT'] = next(weights)
        G[p_id][c_id]['relation'] = random.choice(INFRELATIONS)
    return G

# Build test input 
# Complete digraph with 3 nodes - 6 edges
G1 = nx.DiGraph(directed=True)
sym_ids = range(0, 3)
G1 = construct_graph(G1, sym_ids)

# Complete graph with 2 nodes - 2 edges
G2 = nx.DiGraph(directed=True)
sym_ids = range(0, 2)
G2 = construct_graph(G2, sym_ids)

# graph with 2 nodes - 1 edge (no cycle)
G3 = nx.DiGraph(directed=True)
sym_ids = range(0, 2)
G3.add_nodes_from(sym_ids)
G3.add_edge(0,1)
weights = (i for i in range(6))
G3[0][1]['WEIGHT'] = 2
G3[0][1]['relation'] = random.choice(INFRELATIONS)

# Graph with 1 node - 0 edge
G4 = nx.DiGraph(directed=True)
sym_ids = range(0, 1)
G4 = construct_graph(G4, sym_ids)

# Complete MultiGraph with 3 nodes - 3 edges between each pair of nodes
G5 = nx.MultiDiGraph()
sym_ids = range(0, 3)
G5.add_nodes_from(sym_ids)
edges = list(itertools.permutations(sym_ids, 2))
weights = (i for i in range(6))
for i, (p_id, c_id) in enumerate(edges):
    #avoid self loops
    if p_id == c_id:
        continue
    G5.add_edge(p_id, c_id, WEIGHT=next(weights), relation=random.choice(INFRELATIONS))
    G5.add_edge(p_id, c_id, WEIGHT = random.randint(1, 10), relation=random.choice(INFRELATIONS))
    G5.add_edge(p_id, c_id, WEIGHT = random.randint(1, 10), relation=random.choice(INFRELATIONS))
testdata = [G5, G1, G2, G3, G4]

@pytest.mark.parametrize("G", testdata)
def test_maximum_spanning_arborescence_custom(G):
    print("\n\nInput Graph:")
    print(f"Nodes = {G.nodes()}")
    mst = maximum_spanning_arborescence_custom(G, attr="WEIGHT", relation="relation", default=0.0, 
                                               preserve_attrs=True, verbose=True)
    print("\n\nExtracted MST:")
    print(f"Nodes = {mst.nodes()}")
    print(f"Edges = {mst.edges(data=True)}\n\n")
    print("-------------------------------------------------------------------------------")
