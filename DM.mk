################################################################
# Makefile for Distributed MathSeer Pipeline
#
#
# DM stands for 'Distributed Makefile'. DM is just easier to type.
#
# Matt Langsenkamp 2021
#################################################################

build-aio:
	./bin/build-aio

msp-up: build-aio
	./bin/msp-namespace-up

msp-down:
	./bin/msp-namespace-down

msp-run-example:
	./run-msp-aio --input_dir ./inputs/ACL --output_dir ./outputs/ACL-AIO -d

msp-run-example-2:
	./run-msp-aio --input_dir ./inputs/ACL-2 --output_dir ./outputs/ACL-AIO-2

msp-run-ACL-10:
	./run-msp-aio --input_dir ./inputs/ACL-10 --output_dir ./outputs/ACL-AIO-10

msp-run-ACL-25:
	./run-msp-aio --input_dir ./inputs/ACL-25 --output_dir ./outputs/ACL-AIO-25
