import os


def scanssd_service(server, namespace, i, exposed_port) -> str:
    return scanssd_service_template.format(i=i, namespace=namespace, server=server, exposed_port=exposed_port)


def create_and_save_scanssd_services(servers, namespace, spec_dir):

    os.makedirs(spec_dir, exist_ok=True)
    services = []
    for i, s in enumerate(servers):
        key = ''
        assert len(s.keys()) > 0
        for k in s.keys():
            key = k
        
        service_file = os.path.join(spec_dir, f"{key}_scanssd_{namespace}.yml")
        services.append(f"scanssd-{i}")
        service_spec = scanssd_service(key, namespace, i, s[key]['ports'])
        with open(service_file, 'w+') as f:
            f.write(service_spec)


scanssd_service_template = """apiVersion: v1
kind: Service
metadata:
  name: scanssd-{i}
  namespace: {namespace}
spec:
  selector:
    app: scanssd-{i}
  type: NodePort
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
    nodePort: {exposed_port}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: scanssd-{i}
  namespace: {namespace}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: scanssd-{i}
  template:
    metadata:
      labels:
        app: scanssd-{i}
    spec:
      nodeName: {server}
      containers:
      - name: scanssd-{i}
        image: "dprl/scanssd:latest"
        imagePullPolicy: Always
        ports:
        - containerPort: 80
"""
