from io import BytesIO
from PIL.Image import Image
from pydantic import BaseModel
from pydantic.class_validators import List

from modules.aiopipeline.model.PdfXml import PdfXml


class LoadedPdf(BaseModel):
    file_name: str
    num_pages: int
    pdf: BytesIO

    def to_pdf_xml(self, xml: BytesIO) -> PdfXml:
        return PdfXml(
            file_name=self.file_name,
            pdf=self.pdf,
            xml=xml,
            num_pages=self.num_pages
        )

    class Config:
        arbitrary_types_allowed = True


